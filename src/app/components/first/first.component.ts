import { Component, OnInit } from '@angular/core';
import { first, fromEvent, last, of } from 'rxjs';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {
  ngOnInit(): void {

    //only logs first click them completed
    const clicks = fromEvent(document, 'click');
    const result = clicks.pipe(first());
    result.subscribe(x => console.log(x), err => console.log(err), () => console.log("completed"));

    //can add condtion based first
    //if no values satisfies the condition and observable is completed it will throw error
    //if nothing satisfies we can provide default values
    of(1, 2, 3).pipe(first((x) => x === 5, 10)) //10 is default value here
      .subscribe(x => console.log(x), err => console.log(err), () => console.log("completed"));


    of(1, 2, 3).pipe(last())
      .subscribe(x => console.log(x), err => console.log(err), () => console.log("completed"));

    of(1, 2, 3).pipe(last((x) => x === 2, 10))
      .subscribe(x => console.log(x), err => console.log(err), () => console.log("completed"));
  }
}
