import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  of,
  map,
  mergeMap,
  concatMap,
  concatMapTo,
  switchMap,
  exhaustMap,
  ObservableInput,
  mergeMapTo,
  take,
  interval,
  merge,
  mergeAll,
  concat,
  concatAll
} from 'rxjs';
import {
  ajax
} from 'rxjs/ajax';

@Component({
  selector: 'app-higher-order',
  templateUrl: './higher-order.component.html',
  styleUrls: ['./higher-order.component.scss']
})
export class HigherOrderComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    // of(1, 2, 3, 4)
    //   .pipe(map(el => of(el * 2)))
    //   .subscribe({
    //     next: (data: any) => {
    //       console.log(data)
    //       //subscribe inside subscribe
    //       // data.subscribe({
    //       //   next: (data: any) => console.log(data),
    //       //   error: (err: Error) => console.log(err),
    //       //   complete: () => console.log('completed'),
    //       // });
    //     },
    //     error: (err: Error) => console.log(err),
    //     complete: () => console.log('completed'),
    //   });

    //Merge Map
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     mergeMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`)
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     mergeMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`)
    //     }, (outerValue, innerValue, outerIndex, innerIndex) => {
    //       // outerValue => 1,2,3,4..
    //       // innerValue => data.response
    //       // then inexes
    //       outerValue;
    //       outerIndex;
    //       innerIndex;
    //       return innerValue;
    //     },
    //       1) //if put 1 ,so it will behave as concatmap : 1 api at a time
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     mergeMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`).pipe(
    //         map((data) => {
    //           return data;
    //         })
    //       );
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //Merge map to , so default value will be there in this case default observable
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     mergeMapTo(ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/1`), 2))
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     mergeMap((id: number) => {
    //       return of(id * 10)
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });


    //Concat Map
    //API Responses will be in order
    //and trigger after getting responses
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     concatMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`)
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //Concat Mapto
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     concatMapTo(ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/2`))
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //switch
    //it will trigger id 5 API, all others gets cancelled
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     switchMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`).pipe(
    //         map((data) => {
    //           return data;
    //         })
    //       );
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //exhaust map
    //only id 1 api triggers and all others will gets cancelled
    // of(1, 2, 3, 4, 5)
    //   .pipe(
    //     exhaustMap((id: number) => {
    //       return ajax.getJSON(`https://jsonplaceholder.typicode.com/posts/${id}`).pipe(
    //         map((data) => {
    //           return data;
    //         })
    //       );
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });


    // *****************************************

    //Merge 
    //whatever the value emitted , it will get logged
    // let source1$ = interval(1000).pipe(
    //   map((val) => 'value from source 1 ' + val),
    //   take(2)
    // );
    // let source2$ = interval(500).pipe(
    //   map((val) => 'value from source 2 ' + val),
    //   take(2)
    // );

    //source2 will automatically subscribes
    //instead of this map and mergeAll can use mergemap
    // source1$
    //   .pipe(
    //     map((val) => source2$),
    //     mergeAll()
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // source1$.pipe(mergeMap((val) => source2$)).subscribe((data) => {
    //   console.log(data);
    // });

    // merge(source1$, source2$).subscribe((data) => {
    //   console.log(data);
    // });


    //concat
    let source1$ = interval(1000).pipe(
      map((val) => 'value from source 1 ' + val),
      take(3)
    );
    let source2$ = interval(500).pipe(
      map((val) => 'value from source 2 ' + val),
      take(2)
    );

    source1$
      .pipe(
        map((val) => source2$),
        concatAll()
      )
      .subscribe((data) => {
        console.log(data);
      });

    source1$.pipe(concatMap((val) => source2$)).subscribe((data) => {
      console.log(data);
    });

    //after completing source2$ only source2$ starts
    // concat(source2$, source2$).subscribe((data) => {
    //   console.log(data);
    // });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}
