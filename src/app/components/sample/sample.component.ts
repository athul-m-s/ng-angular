import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable, fromEvent, interval, sample } from 'rxjs';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit, AfterViewInit {
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

    //latest interval value will get when clicks
    //sample 5 sample 20 sample 24 based on click
    this.buttonEvent = fromEvent(
      document,
      'click'
    );

    interval(500)
      .pipe(sample(this.buttonEvent))
      .subscribe({
        next: (data) => console.log('sample' + data),
        error: (err) => console.log('sample' + err),
        complete: () => console.log('sample completed'),
      });


  }
}
