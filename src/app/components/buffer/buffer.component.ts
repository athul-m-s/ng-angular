import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import {
  Observable,
  buffer,
  bufferCount,
  fromEvent,
  interval,
  tap,
  of,
  bufferTime,
  bufferToggle,
  take,
  bufferWhen,
} from 'rxjs';

@Component({
  selector: 'app-buffer',
  templateUrl: './buffer.component.html',
  styleUrls: ['./buffer.component.scss'],
})
export class BufferComponent implements OnInit, OnDestroy, AfterViewInit {
  intervalData: number[] = [];
  showData$!: Observable<Event>;
  constructor() { }

  ngOnInit(): void {
    //Bufferwhen
    // interval(1000)
    //   .pipe(
    //     tap((data) => console.log(data)),
    //     bufferWhen(() => {
    //       return fromEvent(document, 'click');
    //     }),
    //     take(1)
    //   )
    //   .subscribe((data: number[]) => {
    //     console.log(data); //after 5 sec click on screen , it will emit all the buffer value
    //   });
  }

  ngAfterViewInit() {
    this.showData$ = fromEvent(document.getElementById('showButton')!, 'click');
  }

  startInterval() {
    //Buffer
    // interval(1000)
    //   .pipe(
    //     //tap is side effect operator. this uses for logging data etc
    //     tap((data) => console.log(data)),
    //     take(20),
    //     //buffer saves all the emitted values. once the showData$ triggers 
    //     //it will get all the saved data and start save new values
    //     buffer(this.showData$)
    //   )
    //   .subscribe((data: number[]) => {
    //     this.intervalData.push(...data);
    //     console.log(data); //[0,1,2,3,4]  [5,6,7,8]

    //   });

    // ************************************************

    //Buffer Other Example
    //instead of of interval can also use
    // of(1, 2, 3, 4, 5, 6)
    //   .pipe(bufferCount(2))
    //   .subscribe((data) => {
    //     console.log(data);
    //     //[1,2] [3,4] [5,6]
    //   });
    // ************************************************
    //Buffer Count
    // interval(1000)
    //   .pipe(
    //     take(10),
    //     bufferCount(3, 2) //[0,1,2] [2,3,4] [4,5,6]
    //     //bufferCount(3) //[0,1,2] [3,4,5] //fixed array length 3
    //   )
    //   .subscribe((data: number[]) => {
    //     // this.intervalData.push(...data);
    //     console.log(data);
    //   });
    // ************************************************
    //To identify how many triggers or something happens in a timed duration
    //in 5 sec time interval how much clicks happens.
    //resets the click count in each 5 sec
    // const clicks = fromEvent(document, 'click');
    // const buffered = clicks.pipe(bufferTime(5000));
    // buffered.subscribe((x) => console.log(x));

    // ************************************************
    // const clicks = fromEvent(document, 'click');
    // const buffered = clicks.pipe(bufferTime(2000, 5000));
    // buffered.subscribe((x) => console.log(x));
    // ************************************************
    //BufferToggle
    // let opening = interval(6000).pipe(tap(() => console.log('open')));
    // let closing = (data: number) => {
    //   return interval(3000).pipe(tap(() => console.log('close')));
    // };
    // interval(1000)
    //   .pipe(
    //     // tap((data) => console.log(data)),
    //     bufferToggle(opening, closing),
    //     take(3)
    //   )
    //   .subscribe((data) => console.log("final " + data));
    // ************************************************
  }

  ngOnDestroy(): void { }
}
