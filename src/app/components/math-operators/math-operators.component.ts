import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  of,
  count,
  min,
  max,
  scan,
  reduce,
  last,
  isEmpty,
  findIndex,
  find,
  every,
  defaultIfEmpty
} from 'rxjs';

@Component({
  selector: 'app-math-operators',
  templateUrl: './math-operators.component.html',
  styleUrls: ['./math-operators.component.scss']
})
export class MathOperatorsComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    //Count
    //wait till obs completed
    // let source$ = of(1, 2, 3, 4, 5, 6, 7);
    // // source$.pipe(count((val, index) => val > 6)).subscribe((data) => { //1 with condition
    // source$.pipe(count()).subscribe((data) => {
    //   console.log(data); //7
    // });

    //min

    // let source1$ = of(1, 2, 3, 4);
    // source1$.pipe(min()).subscribe((data) => {
    //   console.log(data);
    // });

    // let persons = [
    //   { name: 'Leela', age: 10 },
    //   { name: 'Mathew', age: 30 },
    //   { name: 'John', age: 20 },
    //   { name: 'me', age: 1 },
    // ];

    // of(...persons)
    //   .pipe(min((a, b) => (a.age < b.age ? -1 : 1)))
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //max
    // let source1$ = of(1, 2, 3, 4);
    // source1$.pipe(max()).subscribe((data) => {
    //   console.log(data);
    // });

    // let persons = [
    //   { name: 'Leela', age: 10 },
    //   { name: 'Mathew', age: 30 },
    //   { name: 'John', age: 20 },
    //   { name: 'me', age: 1 },
    // ];

    // of(...persons)
    //   .pipe(max((prev, curr) => (prev.age < curr.age ? -1 : 1)))
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //reduce
    // let source1$ = of(1, 2, 3, 4);
    // source1$.pipe(reduce((acc, curr) => acc + curr, 0)).subscribe((data) => {
    //   console.log(data); // 10
    // });

    //scan
    // let source1$ = of(1, 2, 3, 4);
    // source1$.pipe(scan((acc, curr) => acc + curr, 0)).subscribe((data) => {
    //   // source1$.pipe(scan((acc, curr) => acc + curr, 0), last()).subscribe((data) => {
    //   console.log(data); // 1 3 6 10, if last use then 10
    // });

    //isEmpty
    //if obs emit a single value isempty will set to false and complete the obs
    // let source1$ = of(); //true
    // source1$.pipe(isEmpty()).subscribe((data) => {
    //   console.log(data); // false
    // });

    //findIndex
    //complete obs when index is found
    //-1 if no elements is available
    // let source2$ = of(1, 3, 4, 5, 2);
    // source2$.pipe(findIndex((x) => x === 2)).subscribe((data) => {
    //   console.log(data);
    // });

    //find
    //complete obs when find got a single value
    //undefined if no value is there
    // let source3$ = of(1, 3, 4, 5, 2);
    // source3$.pipe(find((x) => x === 2)).subscribe((data) => {
    //   console.log(data);
    // });

    //every
    // let source4$ = of(3, 4, 5, 2);
    // source4$.pipe(every((x) => x > 1)).subscribe((data) => {
    //   console.log(data);
    // });

    //defaultIfEmpty
    //if obs not emitting any value
    // let source5$ = of();
    // source5$.pipe(defaultIfEmpty(7)).subscribe((data) => {
    //   console.log(data);
    // });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}
