import { Component, OnDestroy } from '@angular/core';
import { Observable, Subscription, from, interval } from 'rxjs';

@Component({
  selector: 'app-cancel-observable',
  templateUrl: './cancel-observable.component.html',
  styleUrls: ['./cancel-observable.component.scss'],
})
export class CancelObservableComponent implements OnDestroy {
  customSubscription: Subscription;
  custom2Subscription: Subscription;
  subscription: Subscription;

  //Creates an Observable that emits sequential numbers every specified interval of time
  public obs$ = interval(2000);

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  //instead of interval using custom observable
  public customObs$ = new Observable((observer) => {
    let i = 0;
    let interval = setInterval(() => {
      console.log('inside custom obs');

      if (i === 3) {
        observer.error('error');
      }

      observer.next(i++);
    }, 2000);

    //This needs to be added in custom observables or else
    //console.log('inside custom obs') this will run due to setInterval
    //But the .next wont trigger
    //this will trigger if unsubscribe is done or error is happened "observer.error('error');""
    return () => {
      clearInterval(interval);
    };
  });

  constructor() {
    this.customSubscription = this.customObs$.subscribe({
      next: (data) => console.log(data),
      error: (err) => console.log('error' + err),
      complete: () => console.log('completed'),
    });

    // this.subscription = this.obs$.subscribe({
    //   next: (data) => console.log(data),
    //   error: (err) => console.log('from error' + err),
    //   complete: () => console.log('completed'),
    // });

    //multiple unsubscribe by
    this.customSubscription.add(this.subscription);
    this.customSubscription.add(this.custom2Subscription);
    //and using untilDestroyed(this);
  }

  cancelSubscription() {
    // this.subscription.unsubscribe();
    this.customSubscription.unsubscribe();
  }
}
