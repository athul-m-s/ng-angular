import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  of,
  toArray,
  take,
  subscribeOn,
  asyncScheduler,
  merge,
  observeOn,
  materialize,
  map,
  ObservableNotification,
  dematerialize,
  delay,
  concatMap
} from 'rxjs';

@Component({
  selector: 'app-utility-operators',
  templateUrl: './utility-operators.component.html',
  styleUrls: ['./utility-operators.component.scss']
})
export class UtilityOperatorsComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {
    //toArray
    // of(1, 2, 3).pipe(take(2), toArray()).subscribe(data => {
    //   console.log(data); //[1,2]
    // })

    //subscribeOn : to pass scheduler , so based on priority can run on micro task or normal task
    //entire subscription will kept it in scheduler
    // console.log('start scrpting');
    // let source$ = of(1, 2).pipe(subscribeOn(asyncScheduler));
    // let source2$ = of(10, 20);

    // merge(source2$, source$).subscribe((data) => {
    //   console.log(data);
    // });

    // console.log('end scripting');

    // start scrpting
    // 10
    // 20
    // end scripting
    // 1
    // 2

    //observeOn : same as subscribeOn, from where we put observeOn, form there it will put it in scheduler
    //placing of observeOn operator matters
    // console.log('start scripting');
    // let source$ = of(1, 2, 3, 45);

    // source$
    //   .pipe(
    //     //map didnt put it on async scheduler
    //     observeOn(asyncScheduler),
    //     map((data) => {
    //       console.log('map executing');
    //       return data * 10;
    //     })
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });
    // console.log('end scripting');

    // map added before observeOn operator
    // start scripting
    // map executing
    // map executing
    // map executing
    // map executing
    // end scripting
    // 10
    // 20
    // 30
    // 450

    // map added after observeOn operator and it will be same for subscribeOn
    // start scripting
    // end scripting
    // map executing
    // 10
    // map executing
    // 20
    // map executing
    // 30
    // map executing
    // 450

    //materialize
    //convert into notification Object
    // let source$ = of(1, 2, 3, 4, 5);

    // source$
    //   .pipe(
    //     map((data) => {
    //       throw 'caught error';
    //     }),
    //     materialize()
    //   )
    //   .subscribe({
    //     next: (data) => {
    //       console.log(data);
    //     },
    //     error: (error) => {
    //       console.log(error);
    //     },
    //     complete: () => {
    //       console.log('complete');
    //     },
    //   });

    //output
    // {
    //   "kind": "N",
    //   "value": 1,
    //   "hasValue": true
    // }
    // {
    //   "kind": "N",
    //   "value": 2,
    //   "hasValue": true
    // }

    //for error : error also converted into notification Object
    // {
    //   "kind": "E",
    //   "error": "caught error",
    //   "hasValue": false,
    //   "value": undefined
    // }

    //deMaterialize
    //conv materialize object into 0 1 2 3 4
    // let obsNotifications: ObservableNotification<number>[] = [];
    // for (let i = 0; i < 5; i++) {
    //   obsNotifications.push({
    //     kind: 'N',
    //     value: i,
    //   });
    // }

    // of(...obsNotifications)
    //   .pipe(dematerialize())
    //   .subscribe({
    //     next: (data) => {
    //       console.log(data);
    //     },
    //     error: (error) => {
    //       console.log(error);
    //     },
    //     complete: () => {
    //       console.log('complete');
    //     },
    //   });

    //delay
    //after 2 second only it will subscribe
    // let source$ = of(1, 2, 3, 4);

    //after 5 sec all values will show
    // source$.pipe(delay(5000)).subscribe((data) => {
    //   console.log(data);
    // });

    // //delay each value emition by 2 sec
    // source$.pipe(concatMap((val) => of(val).pipe(delay(2000)))).subscribe((data) => {
    //   console.log(data);
    // });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}