import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import {
  of,
  skip,
  skipLast,
  interval,
  skipUntil,
  Observable,
  fromEvent,
  skipWhile,
} from 'rxjs';

@Component({
  selector: 'app-skip',
  templateUrl: './skip.component.html',
  styleUrls: ['./skip.component.scss'],
})
export class SkipComponent implements OnInit, AfterViewInit, OnDestroy {
  buttonEvent!: Observable<Event>;
  constructor() { }
  ngOnInit(): void {
    //skip
    // of(1, 2, 3)
    //   .pipe(skip(1)) //skips the first value from start
    //   .subscribe({
    //     next: (data) => console.log('skip' + data),
    //     error: (err) => console.log('skip' + err),
    //     complete: () => console.log('skip completed'),
    //   });

    //skiplast
    // of(1, 2, 3)
    //   .pipe(skipLast(2)) //skips from last
    //   .subscribe({
    //     next: (data) => console.log('skip' + data),
    //     error: (err) => console.log('skip' + err),
    //     complete: () => console.log('skip completed'),
    //   });

    // wait for 2 sec or 5 sec based on number added in skipLast then it will start emitting from 0
    //output after 5 sec skip 0 , skip 1 , skip 2
    //5 values will saves in the buffer and once 6th value is emitted, 
    //it will emit value from buffer that is why 0 1 2 comes instead direct 6
    //skiplast with non-stop values
    // interval(1000)
    //   .pipe(skipLast(5))
    //   .subscribe({
    //     next: (data) => console.log('skip' + data),
    //     error: (err) => console.log('skip' + err),
    //     complete: () => console.log('skip completed'),
    //   });

    //skipwhile
    //if true no emit, if false starts emit 3 4 5 1 2
    // of(1, 2, 3, 4, 5, 1, 2)
    //   .pipe(skipWhile((x) => x < 3))
    //   .subscribe((data) => {
    //     console.log(data);
    //   });
  }

  ngAfterViewInit(): void {
    //skip untill
    this.buttonEvent = fromEvent(
      document.getElementById('takeUntil')!,
      'click'
    );

  }

  startTimer() {
    interval(1000)
      .pipe(skipUntil(this.buttonEvent))
      .subscribe({
        next: (data) => console.log('skip' + data),
        error: (err) => console.log('skip' + err),
        complete: () => console.log('skip completed'),
      });
  }

  ngOnDestroy(): void { }
}
