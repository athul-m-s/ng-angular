import { Component, OnInit } from '@angular/core';
import {
  Observable,
  fromEvent,
  interval,
  of,
  take,
  takeLast,
  takeUntil,
  takeWhile,
} from 'rxjs';

@Component({
  selector: 'app-take',
  templateUrl: './take.component.html',
  styleUrls: ['./take.component.scss'],
})
export class TakeComponent implements OnInit {
  buttonEvent!: Observable<Event>;
  constructor() { }

  ngOnInit(): void {
    //Take
    // of(1, 2, 3, 4, 5)
    //   //even the value is 8, after 5 it will completed
    //   .pipe(
    //     take(1), //only 1 and completes
    //     take(8))
    //   .subscribe({
    //     next: (data) => console.log('take' + data),
    //     error: (err) => console.log('take' + err),
    //     complete: () => console.log('take completed'),
    //   });

    //Takelast
    // of(1, 2, 3, 4, 5)
    // interval(500)
    //   //even the value is 8, after 5 it will completed
    //   .pipe(take(3), takeLast(2))
    //   .subscribe({
    //     next: (data) => console.log('take' + data),
    //     error: (err) => console.log('take' + err),
    //     complete: () => console.log('take completed'),
    //   });
  }

  ngAfterViewInit() {
    this.buttonEvent = fromEvent(
      document.getElementById('takeUntil')!,
      'click'
    );
  }

  startTimer() {
    //takeuntill
    // interval(1000)
    //   .pipe(takeUntil(this.buttonEvent))
    //   .subscribe({
    //     next: (data) => console.log('take ' + data),
    //     error: (err) => console.log('take' + err),
    //     complete: () => console.log('take completed'),
    //   });

    //takewhile
    //true : take 4 then completed
    //false : take 5 then completed
    let asd = true;
    setTimeout(() => {
      asd = false;
      console.log('false');

    }, 3000);
    interval(500)
      .pipe(takeWhile((x, i) => x < 5, true))
      // .pipe(takeWhile(() => asd))
      .subscribe({
        next: (data) => console.log('take' + data),
        error: (err) => console.log('take' + err),
        complete: () => console.log('take completed'),
      });
  }
}
