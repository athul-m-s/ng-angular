import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  of,
} from 'rxjs';
import {
  ajax
} from 'rxjs/ajax';

@Component({
  selector: 'app-ajax',
  templateUrl: './ajax.component.html',
  styleUrls: ['./ajax.component.scss']
})
export class AjaxComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    // this.sub =
    //   ajax('https://jsonplaceholder.typicode.com/todos') //get value as ajaxresponse
    //     // ajax.getJSON('https://jsonplaceholder.typicode.com/todos') //get value in JSON
    //     .pipe()
    //     .subscribe({
    //       next: (data: any) => console.log(data),
    //       error: (err: Error) => console.log(err),
    //       complete: () => console.log('completed'),
    //     });
  }

  ngAfterViewInit(): void {

    ajax({
      url: `https://jsonplaceholder.typicode.com/posts`,
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        test: 'test-text',
      },
      body: {
        title: 'test-check',
      },
    }).subscribe((data) => {
      console.log(data);
    });

  }

  ngOnDestroy(): void {

  }
}