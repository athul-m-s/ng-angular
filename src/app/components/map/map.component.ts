import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  of,
  map,
  mapTo
} from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {
    this.sub = of(1, 2, 3, 4)
      .pipe(map((x) => x * 2))
      .pipe(mapTo('a')) //a a a a
      .subscribe({
        next: (data: any) => console.log(data),
        error: (err: Error) => console.log(err),
        complete: () => console.log('completed'),
      });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}
