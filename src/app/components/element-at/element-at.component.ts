import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  audit,
  elementAt,
  fromEvent,
  interval,
  of,
} from 'rxjs';

@Component({
  selector: 'app-element-at',
  templateUrl: './element-at.component.html',
  styleUrls: ['./element-at.component.scss']
})
export class ElementAtComponent {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {
    //if 10th index is not there it will sent def value 100,
    //if no def value provided it will return the error
    this.sub = of(1, 2, 3, 4, 5)
      .pipe(elementAt(10, 100))
      .subscribe({
        next: (data) => console.log('elementAt' + data),
        error: (err) => console.log('elementAt' + err),
        complete: () => console.log('elementAt completed'),
      });
  }

  ngAfterViewInit(): void {
    //Emit only the third click event
    const clicks = fromEvent(document, 'click');
    const result = clicks.pipe(elementAt(2));
    result.subscribe(x => console.log(x));

    // Results in:
    // click 1 = nothing
    // click 2 = nothing
    // click 3 = MouseEvent object logged to console
  }

  ngOnDestroy(): void {

  }
}
