import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  throttle,
  fromEvent,
  interval,
} from 'rxjs';

@Component({
  selector: 'app-throttle',
  templateUrl: './throttle.component.html',
  styleUrls: ['./throttle.component.scss']
})
export class ThrottleComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

    //user has to wait 3 sec after a click.
    //click will logs console that time itself and 3 sec it wont allow log, then after 3 sec it will allow click
    //like exhaust map, refresh
    // const clicks = fromEvent(document, 'click');
    // const result = clicks.pipe(throttle(() => interval(3000)));
    // result.subscribe(x => console.log(x));

    interval(1000)
      // .pipe(throttle((val) => interval(2000))) //[0,3,6,9..]
      // .pipe(throttle((val) => interval(2000), { leading: true, trailing: false })) //[0,3,6,9..]
      // .pipe(throttle((val) => interval(2000), { leading: true, trailing: true })) //[1,2,4,6,8..]
      // .pipe(throttle((val) => interval(2000), { leading: false, trailing: true })) //[2,4,6,8...]
      .pipe(throttle((val) => interval(2000), { leading: false, trailing: false })) //nothing
      .subscribe({
        next: (data) => console.log('sample' + data),
        error: (err) => console.log('sample' + err),
        complete: () => console.log('sample completed'),
      });
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
