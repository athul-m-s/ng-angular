import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, filter, interval, map, take } from 'rxjs';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.scss'],
})
export class OperatorsComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  public obs$ = interval(1000);
  constructor() { }

  ngOnInit(): void {
    this.subscription = this.obs$
      .pipe(
        filter((el) => el % 2 === 0),
        map((el) => 'number is ' + el),
        take(5)
      )
      .subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log('from error' + err),
        complete: () => console.log('completed'),
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
