import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import {
  Observable,
  Subscription,
  from,
  of,
  groupBy,
  concatMap,
  concatAll,
  mergeMap,
  reduce,
  switchMap,
  exhaustMap,
  tap,
  toArray,
  map,
  pairwise,
  interval,
  window,
  take,
  windowCount,
  windowTime,
  windowToggle,
  bufferToggle,
  windowWhen
} from 'rxjs'

@Component({
  selector: 'app-transformation',
  templateUrl: './transformation.component.html',
  styleUrls: ['./transformation.component.scss']
})
export class TransformationComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    //groupBy
    let posts = [
      {
        id: 1,
        likes: 2,
        comments: 4,
      },
      {
        id: 1,
        likes: 4,
        comments: 5,
      },
      {
        id: 2,
        likes: 5,
        comments: 10,
      },
      {
        id: 3,
        likes: 3,
        comments: 7,
      },
    ];

    // from(posts)
    //   .pipe(
    //     groupBy((val) => val.id),
    //     //3 separate array in response.id 1 object will be in one array , id 2 and 3 will be in 2 separate array
    //     mergeMap(val => val.pipe(toArray()))
    //     // mergeMap((val) =>
    //     //   val.pipe(
    //     //     reduce(
    //     //       (acc, curr) => {
    //     //         acc.id = acc.id || curr.id;
    //     //         acc.likes = acc.likes + curr.likes;
    //     //         acc.comments = acc.comments + curr.comments;
    //     //         return acc;
    //     //       },
    //     //       { id: 0, likes: 0, comments: 0 }
    //     //     )
    //     //   ))
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });


    // let source1$ = of(1, 2, 3, 4, 5).pipe(
    //   groupBy((val) => val % 2), //returns obs
    // )
    //   .subscribe((data) => {
    //     data.pipe(toArray()).subscribe(el => {
    //       console.log(el); //[1,3,5] [2,4]
    //     })
    //   });

    // let source$ = from(posts).pipe(
    //   groupBy((val) => val.id), //returns obs
    //   // tap(val => console.log(val),
    //   // ),
    //   // concatMap((val) => val) //1
    //   // mergeMap((val) => val) //1 2 3 4 5
    //   // switchMap(val => val)//1 2 3 4 5
    //   // exhaustMap(val => val) //1
    // )
    //   .subscribe((data) => {
    //     console.log(data)
    //     // data.pipe(toArray()).subscribe(el => {
    //     //   console.log(el); //[1,3,5] [2,4]
    //     // })
    //   });

    //pairwise
    //minimum 2 values needed or it wont emit

    // let source1$ = of(1, 2, 3, 4, 5, 6, 7,);
    // //just complete will emit for this obs
    // let source$ = of(1);

    // source1$.pipe(pairwise()).subscribe({
    //   next: (data) => {
    //     console.log(data);//[1,2] [2,3] [3,4]..[6,7]
    //   },
    //   error: (error) => {
    //     console.log(error);
    //   },
    //   complete: () => {
    //     console.log('complete');
    //   },
    // });

    //window
    //whenever the inner obs emit it will take all the parent obs emitted values as observable
    //difference is it will emit values as obs 
    //so uses mergemap
    //buffer automatically return as array and it is not obs
    //windows is return as obs and not array
    // let source$ = interval(1000).pipe(take(8));
    // source$
    //   .pipe(
    //     window(interval(4000)),
    //     mergeMap((val) => val.pipe(toArray())) //buffer does this automatically
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //windowCount
    //same as buffer count and it returns obs
    //it wont return as array
    // let source$ = interval(500).pipe(take(5));

    // source$
    //   .pipe(
    //     // windowCount(2, 1), //number of array items , emition item (one item emited by source then create 2 size array)
    //     windowCount(2), //Output when put just 2 [0,1] [2,3] [4]
    //     mergeMap((val) => val.pipe(toArray()))
    //   )
    //   .subscribe((data) => {
    //     console.log(data); //[0,1] [1,2] [2,3] [3,4] [4] []
    //   });

    //windowTime
    // let source$ = interval(500).pipe(take(30))

    // source$
    //   .pipe(
    //     //windowTime(1000), //within 1000millisec whatever values emitted by intervel will show [0] [1,2,3] [4,5] [6,7]....
    //     //windowTime(2000), //within 2000millisec whatever values emitted by intervel will show [0,1,2] [3,4,5,6,7] [8,9,10,11] ...
    //     windowTime(2000, 5000), //in every 5 sec it will emit values, which updated the buffer 2 sec back
    //     //[0, 1, 2]
    //     // [9,10,11,12,13]
    //     // [19,20,21,22,23]
    //     //...................
    //     mergeMap((val) => val.pipe(toArray()))
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //windowToggle
    //after 4 sec opening interval 0 emits and it starts, wait for 500msec and emits 3
    //then again after 4 sec opening interval 1 emits and wait 500msc and it emits 7
    //then 5th second 
    // let source$ = interval(1000).pipe(take(15))

    // source$
    //   .pipe(
    //     windowToggle(interval(4000).pipe(tap(val => console.log('this value is inner interval ' + val)
    //     )), () => interval(500)),//opening and closing, when to start and when to stop
    //     mergeMap((val) => val.pipe(tap(x => console.log('this value is outer interval ' + x)
    //     ), toArray()))
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // [3]
    // [7]
    // [11]

    //windowWhen
    //every two seconds it will emit source observable values
    //but returns as obs
    // let source$ = interval(500).pipe(take(10))

    // source$
    //   .pipe(
    //     windowWhen(() => interval(2000)),
    //     mergeMap((val) => val.pipe(toArray()))
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // [0, 1, 2]
    // [3, 4, 5, 6, 7]
    // [8, 9]

  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}