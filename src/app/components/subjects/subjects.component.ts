import { outputAst } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subject,
  Subscription,
  of,
  fromEvent,
  interval,
  connectable,
  share,
  BehaviorSubject,
  ReplaySubject,
  AsyncSubject,
  take
} from 'rxjs';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    // let observable$ = new Observable<number>((observer) => {
    //   observer.next(1);
    //   observer.next(2);
    //   observer.next(3);
    //   observer.complete();
    // });

    // let observer1 = {
    //   next: (data: number) => {
    //     console.log('observer 1 - ' + data);
    //   },
    //   error: (error: any) => {
    //     console.log(error);
    //   },
    //   complete: () => {
    //     console.log('observer 1 complete');
    //   },
    // };

    // let observer2 = {
    //   next: (data: number) => {
    //     console.log('observer 2 - ' + data);
    //   },
    //   error: (error: any) => {
    //     console.log(error);
    //   },
    //   complete: () => {
    //     console.log('observer 2 complete');
    //   },
    // };

    // observable$.subscribe(observer1);
    // observable$.subscribe(observer2);

    //output
    // observer 1 - 1
    // observer 1 - 2
    // observer 1 - 3
    // observer 1 complete
    // observer 2 - 1
    // observer 2 - 2
    // observer 2 - 3
    // observer 2 complete



    this.subjectAsObservable();

  }

  ngAfterViewInit(): void {

  }

  subjectAsObservable() {

    // let observable$ = of(1, 2); //execute one tinme
    // let subject = new Subject<number>();

    // subject.subscribe({
    //   next: (data: number) => {
    //     console.log('observer 1 -' + data);
    //   },
    //   error: (error: any) => {
    //     console.log(error);
    //   },
    //   complete: () => {
    //     console.log('observer 1 complete');
    //   },
    // });
    // subject.subscribe({
    //   next: (data: number) => {
    //     console.log('observer 2 - ' + data);
    //   },
    //   error: (error: any) => {
    //     console.log(error);
    //   },
    //   complete: () => {
    //     console.log('observer 2 complete');
    //   },
    // });

    // // observable sends data to subject and from subjects we subscribe again
    // observable$.subscribe(subject);

    //output
    // observer 1 - 1
    // observer 2 - 1
    // observer 1 - 2
    // observer 2 - 2
    // observer 1 complete
    // observer 2 complete

    let subject = new Subject<number>();
    subject.next(1);
    subject.error(new Error('test')) //if error happens no next step
    subject.next(2);
    subject.complete();

    //observer 1 - 1
    // observer 2 - 1
    // observer 1 - 2
    // observer 2 - 2
    // observer 1 complete
    // observer 2 complete


    //Cold Observable
    // let Obs$ = new Observable((observer) => {
    //   observer.next(Math.random()); //value created inside obs
    // });

    // Obs$.subscribe((value) => {
    //   console.log('observer 1' + value);
    // });
    // Obs$.subscribe((value) => {
    //   console.log('observer 2' + value);
    // });

    // //Hot Observable
    // let num = Math.random();
    // let Obs1$ = new Observable((observer) => {
    //   observer.next(num); //value created outside obs
    // });

    // Obs1$.subscribe((value) => {
    //   console.log('observer 1' + value);
    // });
    // Obs1$.subscribe((value) => {
    //   console.log('observer 2' + value);
    // });

    //Hot Observable : bcoz fromEvent is hot
    // let documentEvent$ = fromEvent(document, 'click');

    // documentEvent$.subscribe((data: Event) => {
    //   console.log((data as PointerEvent).clientX);
    // });

    // documentEvent$.subscribe((data: Event) => {
    //   console.log((data as PointerEvent).clientX);
    // });

    // convert cold to hot
    //cold
    // let interval$ = interval(1000); //interval is coldobservable

    // interval$.subscribe((data) => {
    //   console.log('Observer 1 ' + data);
    // });

    // setTimeout(() => {
    //   interval$.subscribe((data) => {
    //     console.log('observer 2 ' + data);
    //   });
    // }, 2000);

    //Convert cold to hot using subject
    // let interval1$ = interval(1000); //coldobservable
    // let subject$ = new Subject();

    // interval1$.subscribe(subject$);

    // subject$.subscribe((data) => {
    //   console.log('Observer 1 ' + data);
    // });

    // setTimeout(() => {
    //   subject$.subscribe((data) => {
    //     console.log('observer 2 ' + data);
    //   });
    // }, 2000);

    //Convert cold to hot using connectable
    // let asd = connectable(interval(1000));
    // asd.subscribe((data) => {
    //   console.log(data);
    // });

    // setTimeout(() => {
    //   asd.subscribe((data) => {
    //     console.log(data);
    //   });
    // }, 2000);

    // asd.connect()

    //connectable with behavious subject

    //first observer 1 will emit with 100, then 1 2
    // let source$ = connectable(interval(1000), {
    //   connector: () => new BehaviorSubject(100),
    // });

    // source$.subscribe((data) => {
    //   console.log('observer 1 ' + data);
    // });
    // setTimeout(() => {
    //   source$.subscribe((data) => {
    //     console.log('observer 2 ' + data);
    //   });
    // }, 4000);

    // source$.connect();

    // observer 1 100
    // observer 1 0
    // observer 1 1
    // observer 1 2
    // observer 2 2
    // observer 1 3
    // observer 2 3

    //connectable with async subject

    // let source$ = connectable(interval(1000).pipe(take(5)), {
    //   connector: () => new AsyncSubject(),
    // });

    // source$.subscribe((data) => {
    //   console.log('observer 1 ' + data);
    // });

    // setTimeout(() => {
    //   source$.subscribe((data) => {
    //     console.log('observer 2' + data);
    //   });
    // }, 4000);

    // source$.connect();

    // observer 1 4
    // observer 2 4

    //connectable with Replay subject
    // let source$ = of(1, 2, 3).pipe(
    //   share({ connector: () => new ReplaySubject(1) })
    // );

    // source$.subscribe((data) => {
    //   console.log('observer 1 ' + data);
    // });
    // setTimeout(() => {
    //   source$.subscribe((data) => {
    //     console.log('observer 2 ' + data);
    //   });
    // }, 4000);

    //output
    // observer 1 1
    // observer 1 2
    // observer 1 3
    // observer 2 1
    // observer 2 2
    // observer 2 3

    //Convert cold to hot using share
    // let asd = interval(1000).pipe(take(5), share());

    // asd.subscribe((data) => {
    //   console.log(data);
    // });

    // setTimeout(() => {
    //   asd.subscribe((data) => {
    //     console.log(data);
    //   });
    // }, 2000);

    //Behavious subjects :
    // let behaviorSubject$ = new BehaviorSubject(0);

    // behaviorSubject$.subscribe((data) => {
    //   console.log('observer 1 - ' + data);
    // });

    // behaviorSubject$.next(1); //

    // behaviorSubject$.subscribe((data) => {
    //   console.log('observer 2 - ' + data); //returns 1
    // });

    // behaviorSubject$.next(2); //both return 2

    //output
    // observer 1 - 0
    // observer 1 - 1
    // observer 2 - 1
    // observer 1 - 2
    // observer 2 - 2

    //ReplaySubject : def buff size is infinity, and time is milli seconds , how much time the value should be hold
    // let replaySubject$ = new ReplaySubject(10, 4000);//buffersize and time as params , are optional
    // let replaySubject$ = new ReplaySubject();
    // replaySubject$.next(1);
    // replaySubject$.next(2);

    // replaySubject$.subscribe((data) => {
    //   console.log('observer 1 -' + data); //it will emit 1 and 2 here
    // });

    // setTimeout(() => {
    //   replaySubject$.subscribe((data) => {
    //     console.log('observer 2 -' + data); //it will emit 1 and 2
    //   });
    //   replaySubject$.next(5); //obs1 and obs2 will emit 5
    // }, 5000);

    // observer 1 - 1
    // observer 1 - 2
    // //after 5 sec
    // observer 2 - 1
    // observer 2 - 2
    // observer 1 - 5
    // observer 2 - 5

    //AsyncSubject
    // let asyncSubject$ = new AsyncSubject();

    // asyncSubject$.subscribe((data) => {
    //   console.log('observer 1 ' + data);
    // });

    // asyncSubject$.next(1);
    // asyncSubject$.next(2);
    // asyncSubject$.next(3);

    // setTimeout(() => {
    //   asyncSubject$.subscribe((data) => {
    //     console.log('observer 2 ' + data);
    //   });
    //   asyncSubject$.complete();
    // }, 3000);

    //after 3 sec when it completes
    //observer 1 3
    //observer 2 3


    //Void Subject : no need of value, just emits thats all
    // let subject3$ = new Subject<void>();

    // subject3$.subscribe((data) => {
    //   console.log(data);
    // });

    // subject3$.next();


    //share : to convert to cold to hot
    // const source = interval(1000).pipe(
    //   share()
    // );

    // source.subscribe(x => console.log('subscription 1: ', x));
    // setTimeout(() => {
    //   source.subscribe(x => console.log('subscription 2: ', x));
    // }, 2000);
  }

  ngOnDestroy(): void {

  }
}