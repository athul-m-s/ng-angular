import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  catchError,
  delay,
  delayWhen,
  interval,
  map,
  of,
  retry,
  retryWhen,
  take,
  tap,
  timer
} from 'rxjs';

@Component({
  selector: 'app-error-handling',
  templateUrl: './error-handling.component.html',
  styleUrls: ['./error-handling.component.scss']
})
export class ErrorHandlingComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    //catchError
    // let source$;
    // this.sub = source$ = new Observable((observer) => {
    //   observer.next(1)
    //   observer.next(2)
    //   observer.error(new Error('error'))
    // }).pipe(catchError((err, sourceObs) => {
    //   console.log('inside catch error ' + err);
    //   // return err;
    //   throw err
    //   // return sourceObs; //then should add take(2) or else infinite loop happens
    //   // return of(1, 2, 3) // if error happens handles by sending something else
    // })).subscribe({
    //   next: (data: any) => console.log(data),
    //   error: (err: Error) => console.log('error console ' + err),
    //   complete: () => console.log('completed'),
    // });

    //return err : error console TypeError: You provided an invalid object
    //where a stream was expected. You can provide an Observable, Promise, ReadableStream,
    //Array, AsyncIterable, or Iterable.

    //throw err : error console Error: error


    //retry
    // let source$ = new Observable((observer) => {
    //   observer.next(1)
    //   observer.next(2)
    //   observer.error(new Error('error'))
    // }).pipe(retry(2), catchError((err, sourceObs) => {
    //   console.log('inside catch error ' + err);
    //   return err;
    // })).subscribe({
    //   next: (data: any) => console.log(data),
    //   error: (err: Error) => console.log('error console ' + err),
    //   complete: () => console.log('completed'),
    // });

    //1 2 1 2 1 2 error


    //retryWhen
    const result = interval(1000).pipe(
      tap(value => {
        if (value > 2) {
          // error will be picked up by retryWhen
          throw value;
        }
        return value;
      }),
      retryWhen(errors =>
        errors.pipe(
          // // log error message
          tap(value => console.log(`Value ${value} was too high!`)),
          // restart in after 2 seconds
          delayWhen(value => timer(2000)),
          take(2)
        )
      )
    ).subscribe({
      next: (data: any) => console.log(data),
      error: (err: Error) => console.log('error console ' + err),
      complete: () => console.log('completed'),
    })

    // 0
    // 1
    // 2
    // Value 3 was too high!
    // 0
    // 1
    // 2
    // Value 3 was too high!
    // completed

  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}