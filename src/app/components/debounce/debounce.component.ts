import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  debounce,
  fromEvent,
  interval

} from 'rxjs';

@Component({
  selector: 'app-debounce',
  templateUrl: './debounce.component.html',
  styleUrls: ['./debounce.component.scss']
})
export class DebounceComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

    //user clicks a button 100 times and stopped click, 
    //so the 100th click will be outputted after 2 second, all other clicks are cancelled
    //input type user keep on typing then after stop 1 sec after trigger api
    let buttonEvent = fromEvent(
      document.getElementById('button_debounce')!,
      'click'
    );

    buttonEvent.pipe(debounce((value) => interval(2000))).subscribe((data) => {
      console.log(data);
    });
  }

  ngOnDestroy(): void {

  }
}


