import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import {
  distinct,
  distinctUntilChanged,
  distinctUntilKeyChanged,
  from,
  of,
} from 'rxjs';

@Component({
  selector: 'app-distinct',
  templateUrl: './distinct.component.html',
  styleUrls: ['./distinct.component.scss'],
})
export class DistinctComponent implements OnInit, OnDestroy, AfterViewInit {
  constructor() { }
  ngOnInit(): void {
    //Distinct => 1,2,3,4,5
    // of(1, 2, 3, 4, 2, 3, 5)
    // from([
    //   { id: 1, name: 'aaa' },
    //   { id: 2, name: 'bbb' },
    //   { id: 1, name: 'aaa' },
    // ])
    //   .pipe(distinct((x) => x.id))
    //   .subscribe({
    //     next: (data) => console.log(data),
    //     error: (err) => console.log(err),
    //     complete: () => console.log('distinct completed'),
    //   });

    //Distinct untill changes => 1, 2, 3, 5
    //only compare with prev value
    of(1, 2, 2, 2, 2, 3, 5)
    from([
      { id: 1, name: 'aaa' },
      { id: 2, name: 'bbb' },
      { id: 1, name: 'ccc' },
      { id: 1, name: 'ddd' },
      { id: 3, name: 'eee' },
    ])
      // .pipe(distinctUntilChanged((prev, curr) => curr.id !== prev.id)) //show only id 1, true then skipped
      .pipe(
        distinctUntilChanged(
          (prev, curr) => curr !== prev,
          (key) => key.id //so prev.id not needed
        )
      ) //show only id 1, true then skipped
      .subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log(err),
        complete: () => console.log('distinct completed'),
      });

    //DistinctUntillKeyChanges : Object filtering, comparison with prev item only using a property
    // id 1 and 2 gets
    // from([
    //   { id: 1, name: 'aaa' },
    //   { id: 2, name: 'aaa' },
    //   { id: 3, name: 'bbb' },
    //   { id: 4, name: 'ccc' },
    //   { id: 5, name: 'cccdd' },
    // ])
    //   .pipe(distinctUntilKeyChanged('name'))
    //   // .pipe(distinctUntilKeyChanged('id'))
    //   // .pipe(
    //   //   distinctUntilKeyChanged('name', (prev, curr) => curr.includes(prev))
    //   // )
    //   .subscribe({
    //     next: (data) => console.log(data), //object [1,3,4]
    //     error: (err) => console.log(err),
    //     complete: () => console.log('distinct completed'),
    //   });
  }
  ngAfterViewInit(): void { }

  ngOnDestroy(): void { }
}
