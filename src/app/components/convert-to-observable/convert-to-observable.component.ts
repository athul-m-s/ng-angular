import { AfterViewInit, Component } from '@angular/core';
import { EMPTY, concatMap, defer, from, fromEvent, generate, interval, of, range, timer } from 'rxjs';

@Component({
  selector: 'app-convert-to-observable',
  templateUrl: './convert-to-observable.component.html',
  styleUrls: ['./convert-to-observable.component.scss'],
})
export class ConvertToObservableComponent {
  public arr = [
    {
      id: 1,
      name: 'test',
    },
    {
      id: 2,
      name: 'test1',
    },
  ];

  //Converting array to observable
  public arr$ = from(this.arr);

  promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('resolved');
      // reject(new Error('error'));
    }, 2000);
  });

  //Converting promise to observable
  public promise$ = from(this.promise);

  public listNumber$ = of(1, 2, 3, 4, 5);

  constructor() {
    // this.arr$.subscribe({
    //   next: (data) => console.log(data),
    //   error: (err) => console.log('from error' + err),
    //   complete: () => console.log('completed'),
    // });

    //If error happens wont trigger the complete function
    // this.promise$.subscribe({
    //   next: (data) => console.log('promise' + data),
    //   error: (err) => console.log('from promise' + err),
    //   complete: () => console.log('from promise completed'),
    // });
  }

  ngAfterViewInit(): void {

    //Converting dom event to observable
    // fromEvent(document.getElementById('link-id')!, 'click').subscribe({
    //   next: (data) => console.log(data),
    //   error: (err) => console.log('event' + err),
    //   complete: () => console.log('event completed'),
    // });

    //Defer
    //based on condition can create observable
    // const clicksOrInterval = defer(() => {
    //   return Math.random() > 0.5
    //     ? fromEvent(document, 'click')
    //     : interval(1000);

    //   //to return empty obs
    //   return EMPTY;

    //   //return keyword should use if using if else block
    // });
    // clicksOrInterval.subscribe(x => console.log(x));

    //range
    //it will wait to emit all the 10 items then only log data all at once.
    //5 means , it will start emitting from 5. so //5 6 7 8 9... total 10 items
    // range(5, 10).subscribe(data => {
    //   console.log(data);
    // })

    //Generate
    //create observable like a forloop
    // const result = generate(0, x => x < 3, x => x + 1, x => x);
    // result.subscribe(x => console.log(x)); //0 1 2 complete

    // const result2 = generate({
    //   initialState: 0,
    //   condition(value) { return value < 3; },
    //   iterate(value) { return value + 1; },
    //   resultSelector(value: any) { return value * 1000; }
    // });

    // result2.subscribe({
    //   next: value => console.log(value),
    //   complete: () => console.log('Complete!')
    // });



    //interval
    //start emit value after 5 sec, and each value emit 5 sec diff
    // interval(5000).subscribe((data) => {
    //   console.log(data);
    // });

    //Timer
    //means, first emit will happen suddenly then in each second emit values 1,2,3,4.. 
    // timer(0, 1000).subscribe((data) => {
    //   console.log(data);
    // });

    //after 5 sec it will emit 0 then complete
    // timer(5000).subscribe((data) => {
    //   console.log(data);
    // });


    // let data$ = of('a', 'b', 'c');

    // setTimeout(() => {
    //   data$.subscribe((data) => {
    //     console.log(data);
    //   });
    // }, 3000);

    // timer(3000)
    //   .pipe(concatMap(() => data$))
    //   .subscribe((data) => {
    //     console.log(data); //after 3 sec a b c emits
    //   });

    //interval wont allow to configure when the first value should emit
    // in timer we can configure when the first value should emit 


  }
}
