import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { filter, of, from, Subscription, fromEvent, tap } from 'rxjs';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit, OnDestroy, AfterViewInit {
  eventSubscription!: Subscription;
  ngOnInit(): void {

    //filter
    from([
      { id: 1, name: 'aaa' },
      { id: 2, name: 'aaa' },
      { id: 3, name: 'bbb' },
      { id: 4, name: 'ccc' },
      { id: 5, name: 'cccdd' },
    ])
      .pipe(filter((x) => x.name === 'aaa'))
      // of(1, 2, 3)
      //   .pipe(filter((x) => x === 2))
      .subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log('filter' + err),
        complete: () => console.log('filter completed'),
      });
  }
  ngAfterViewInit(): void {
    //subscribe only when user clicks some text which has test keyword
    this.eventSubscription = fromEvent(document, 'click')
      .pipe(
        tap((x) => {
          x;
        }),
        filter((event: Event) => {
          return (event.target as HTMLElement).innerText === 'Test';
        })
      )
      .subscribe((data) => {
        console.log(data);
      });
  }
  ngOnDestroy(): void {
    this.eventSubscription.unsubscribe();
  }
}
