import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  asapScheduler,
  asyncScheduler,
  merge,
  of,
  queueScheduler,
} from 'rxjs';

@Component({
  selector: 'app-schedulers',
  templateUrl: './schedulers.component.html',
  styleUrls: ['./schedulers.component.scss']
})
export class SchedulersComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    console.log('script starting');
    let queueScheduler$ = of('Queue Scheduler', queueScheduler);
    let asyncScheduler$ = of('Async Scheduler', asyncScheduler);
    let asapScheduler$ = of('Asap Scheduler', asapScheduler);

    merge(asyncScheduler$, queueScheduler$, asapScheduler$).subscribe(
      (data) => {
        console.log(data);
      }
    );
    console.log('script Ending');

    //  script starting
    //  Queue Scheduler //sync
    //  script Ending
    //  Asap Scheduler //micro-task
    //  Async Scheduler //task-queue
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}