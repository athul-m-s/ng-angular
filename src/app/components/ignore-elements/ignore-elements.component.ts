import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  ignoreElements,
  interval,
  of,
  take,
} from 'rxjs';

@Component({
  selector: 'app-ignore-elements',
  templateUrl: './ignore-elements.component.html',
  styleUrls: ['./ignore-elements.component.scss']
})
export class IgnoreElementsComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {
    //it wont show any observable values, instead it will show completed or not
    //it will wait untill the obs is completed then only completed triggers
    // this.sub = of(1, 2, 3, 4, 5)
    this.sub = interval(1000)
      .pipe(take(5), ignoreElements())
      .subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log(err),
        complete: () => console.log('completed'),
      });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}
