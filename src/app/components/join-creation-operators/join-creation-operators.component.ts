import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  combineLatest,
  combineLatestAll,
  concat,
  concatAll,
  concatMap,
  exhaustAll,
  filter,
  forkJoin,
  from,
  fromEvent,
  interval,
  map,
  merge,
  mergeAll,
  of,
  partition,
  race,
  startWith,
  switchAll,
  take,
  tap,
  timer,
  toArray,
  withLatestFrom,
  zip,
} from 'rxjs';

@Component({
  selector: 'app-join-creation-operators',
  templateUrl: './join-creation-operators.component.html',
  styleUrls: ['./join-creation-operators.component.scss']
})
export class JoinCreationOperatorsComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    //Combine latest
    // let source1$ = fromEvent(document.getElementById('a')!, 'click');

    // let source2$ = fromEvent(document.getElementById('b')!, 'click');

    // //to trigger this, both observable should emit one value atleast.
    // //so both button should click once
    // //only take latest values and emit as an array
    // //one obs emitted error it will stop
    // combineLatest([source1$, source2$]).subscribe((data) => {
    //   console.log(data);
    // });

    //Concat

    ///completes first obs then 2nd starts
    //if error in first no second
    // const timer1 = interval(1000).pipe(take(3));
    // const timer2 = interval(1000).pipe(take(2));

    // concat(timer1, timer2).subscribe((data) => {
    //   console.log(data);
    // });

    // 0 1 2 0 1

    //forkJoin
    //atleast one value should be emitted
    //if error happens it will show error.
    //wait for all obs to complete and combine last emitted values and give values then complete triggers
    // const observable = forkJoin([
    //   of(1, 2, 3, 4),
    //   Promise.resolve(8),
    //   timer(4000)
    // ]);

    // observable.subscribe({
    //   next: value => console.log(value),
    //   complete: () => console.log('This is how it ends!'),
    // });

    //after 4 sec emit [4,8,0]

    //merge
    //all triggers will be emits values. no need to complete the obs, it will trigger whatever it emits
    // let source1$ = fromEvent(document.getElementById('a')!, 'click');

    // let source2$ = fromEvent(document.getElementById('b')!, 'click');

    // let source3$ = of(1, 2, 3)


    // merge(source1$, source2$, source3$).subscribe((data) => {
    //   console.log(data);
    // });

    //partition
    //convert one obs to another based on conditions
    // let obs$ = of(1, 2, 3, 4);

    // let data = partition(obs$, (value) => value % 2 === 0);

    // //separate way
    // //satisfies the condition
    // data[0].subscribe((d) => {
    //   console.log("a " + d); // 2 4
    // });

    // // //not  satisfies the condition
    // data[1].subscribe((d) => {
    //   console.log("b " + d); // 1 3
    // });

    //a 2
    //a 4
    //b 1
    //b 3

    //single way
    // from(data).pipe(concatMap(d => d), toArray()).subscribe(val => {
    //   console.log(val); // [2,4,1,3]
    //   //without toArray 2 4 1 3
    // })

    //Race
    //which emit first value will takes, all others unsubscribe
    //error happens then error throws
    //can check which endpoint recieves data first
    //if 2nd or 3rd obs throw error then it wont take it to consideration
    // if first emitted value emits error then it shows error 

    // let obs1$ = of(1, 2, 3, 4);
    // let obs2$ = of(5, 6, 7, 8);
    // let obs1$ = interval(1000).pipe(take(3));
    // let obs2$ = interval(2000).pipe(take(3));
    // let source2$ = interval(500).pipe(
    //   tap((v) => {
    //     throw 'error'; //this trigger first and error shows , nothing else
    //   })
    // );


    // race(obs1$, obs2$, source2$).subscribe((data) => {
    // race(obs1$, obs2$).subscribe((data) => { //obs1 values shows
    //   console.log(data);
    // }, (err) => console.log(err));

    //zip
    //combine multi obs to one in order
    //if one obs emit extra it wont take it untill next one emits
    //every obs should emit a single value
    //combine latest no need pair but zip needs
    // let obs1$ = of(1, 2, 3, 4, 5, 6);
    // let obs2$ = of(5, 6, 7, 8, 9);
    // let source1$ = fromEvent(document.getElementById('a')!, 'click');
    // zip(obs1$, obs2$, source1$).subscribe((data) => {
    //   //zip(obs1$, obs2$).subscribe((data) => {
    //   console.log(data); //[1,5] [2,6] [3,7]
    // }, (err) => console.log(err));

    // zip([obs1$, obs2$], (a, b) => a + b).subscribe((data) => {
    //   console.log(data); //[6] [8] [10]
    // }, (err) => console.log(err));

    //on each click [1,5. pointerevent]
    //on each click [2,6. pointerevent]
    //............

    //combineLatestAll
    //takes observables or observables
    // let source$ = of('a', 'b');

    // source$
    //   .pipe(
    //     map((value) => {
    //       return interval(1000).pipe(take(2)); //inner obs create for a and b
    //     }),
    //     combineLatestAll() // take latest value from inner obs and emit , works for inner obs
    //   )
    //   .subscribe((data) => {
    //     console.log(data);//[0, 0][1, 0][1, 1]
    //   });

    //first inner obs (interval) 0 and second inner obs 0 // a emit waited for b and  0 0
    //first inner obs 1 and second inner obs 0 // a emit b is already 0 so 1 0
    //first inner obs 1 and second inner obs 1 // b emit a is already 1 so 1 1

    //ConcatAll
    // let source$ = of(1, 2, 3);
    // source$
    //   .pipe(
    //     map((value) => interval(500).pipe(take(value))),
    //     concatAll()
    //   )
    //   .subscribe((data) => {
    //     console.log(data); // 0(1) 0 1(2) 0 1 2(3)
    //   });

    // //in each click it will log 0 and 1 in 1 sec interval
    // //if user clicks 10 times, in 10 times it will log 0 1 in 1 sec time diff
    // const clicks = fromEvent(document, 'click');
    // const higherOrder = clicks.pipe(
    //   map(() => interval(1000).pipe(take(2)))
    // );
    // const firstOrder = higherOrder.pipe(concatAll());
    // firstOrder.subscribe(x => console.log(x));

    //exhaustAll
    // let source$ = interval(1000).pipe(
    //   filter((value) => value > 0),
    //   take(4)
    // );

    // source$
    //   .pipe(
    //     map((value) =>
    //       interval(500).pipe(
    //         map(
    //           (val) =>
    //             `parent interval value ${value} with child interval ${val}`
    //         ),
    //         take(2)
    //       )
    //     ),
    //     exhaustAll() // if uses concatAll it will have 2 also
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //2 & 4 is not logged bcz,while running first (1)'s child obs,  2 gets cancelled, while running 3rd obs child 4 is cancelled
    // parent interval value 1 with child interval 0
    // parent interval value 1 with child interval 1
    // parent interval value 3 with child interval 0
    // parent interval value 3 with child interval 1

    //switchAll
    // let source$ = interval(1000).pipe(
    //   filter((value) => value > 0),
    //   take(5)
    // );

    // source$
    //   .pipe(
    //     map((value) =>
    //       interval(1500).pipe(
    //         map(
    //           (val) =>
    //             `parent interval value: ${value} with child interval ${val}`
    //         ),
    //         take(2)
    //       )
    //     ),
    //     switchAll()
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    //1 2 3 4 will get cancelled or unsubscribes when new obs comes

    // parent interval value: 5 with child interval 0
    // parent interval value: 5 with child interval 1


    //MergeAll
    //no order, parent obs emit, child starts, in that time second parent obs start
    //so without any order it will emits child obs
    // let source$ = interval(1000).pipe(take(2));

    // source$
    //   .pipe(
    //     map((value) =>
    //       interval(1000).pipe(
    //         map(
    //           (val) =>
    //             `Parent interval with value ${value} and child interval of ${val}`
    //         ),
    //         take(2)
    //       )
    //     ),
    //     mergeAll()
    //     //mergeAll(1) //emit one by one as order
    //   )
    //   .subscribe((data) => {
    //     console.log(data);
    //   });

    // parent interval value: 0 with child interval 0
    // parent interval value: 1 with child interval 0
    // parent interval value: 0 with child interval 1
    // parent interval value: 1 with child interval 1

    //mergeAll(1)
    // parent interval value: 0 with child interval 0
    // parent interval value: 0 with child interval 1
    // parent interval value: 1 with child interval 0
    // parent interval value: 1 with child interval 1

    //StartWith
    //before showing interval obs values it will show 10 20 30
    // let source$ = interval(1000).pipe(take(2));

    // source$.pipe(startWith(...[10, 20, 30])).subscribe((data) => {
    //   console.log(data); //10 20 30 0 1
    // });

    //withLatestFrom
    //it emit only when parent obs emits,
    //parent emits and it check which is latest child value then emit the value.
    //child is not restarting when new parent triggers. it is same 
    // let source$ = interval(1000).pipe(take(3));

    // source$.pipe(withLatestFrom(interval(100).pipe(tap(val => console.log(val)
    // )))).subscribe((data) => {
    //   console.log(data); // [0,9] [1,19] [2,29]
    // });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}