import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-new-observable',
  templateUrl: './new-observable.component.html',
  styleUrls: ['./new-observable.component.scss'],
})
export class NewObservableComponent implements OnInit {
  constructor() { }

  ngOnInit() {
    const newObservable = new Observable((observer) => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      //if error triggers it wont run complete
      // observer.error('err');
      observer.complete();
      //it wont work. because after complete next wont trigger
      observer.next(4);
    });

    newObservable.subscribe({
      next: (data) => console.log(data),
      error: (err) => console.log('from error ' + err),
      complete: () => console.log('completed'),
    });

    let localObserver = {
      next: (data: any) => console.log('observer ' + data),
      error: (err: string) => console.log('observer error ' + err),
      complete: () => console.log('observer completed'),
    };

    //Subscribing in different way
    newObservable.subscribe(localObserver);
  }
}
