import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  audit,
  fromEvent,
  interval,
  take,
} from 'rxjs';

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {


    //click on a screen but the log will show up in screen after 2 sec
    // const clicks = fromEvent(document, 'click');
    // const result = clicks.pipe(audit(ev => interval(2000)));
    // result.subscribe(x => console.log(x));

    this.sub = interval(1000)
      .pipe(audit((val) => interval(2000)), take(2))
      .subscribe({
        next: (data) => console.log('sample' + data),
        error: (err) => console.log('sample' + err),
        complete: () => console.log('sample completed'),
      });

    //waited for 2 sec and emited 2 then 2 sec waited and 5
    //output: 2, 5, completed
    //second intervel emits recent value of  first interval in every 2 sec
    // interval emit 0 -> audit starts  -> interval -> 2000 , after two seconds 2 emits
    // interval emit 3 -> audit starts-> interval -> 2000, after two seconds 5
    // 6 -> audit -> interval -> 2000
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
