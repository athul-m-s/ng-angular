import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {
  Observable,
  Subscription,
  single,
  of
} from 'rxjs';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  buttonEvent!: Observable<Event>;

  ngOnInit(): void {

    //it will emit only 1 value,
    //of(1,2) it will show error bcz source observable should not have multiple values

    this.sub = of(1, 2, 4)
      // .pipe(single((val) => val % 2 === 0)) error bcz 2 and 4
      .pipe(single((val) => val % 2 === 1)) // no error, output is 1 and completed
      // .pipe(single((val) => val > 5)) //error 
      .subscribe({
        next: (data: any) => console.log(data),
        error: (err: Error) => console.log(err),
        complete: () => console.log('completed'),
      });
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {

  }
}