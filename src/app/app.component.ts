import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { concat, concatMap, exhaustMap, from, fromEvent, mergeMap, of, switchMap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ng-rxjs';

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    // this.triggerApi();
  }


  triggerApi() {
    of(1, 2, 3, 4, 5, 6, 7, 8, 9)
      .pipe(
        exhaustMap((id: number) => {
          return this.http.get(`https://jsonplaceholder.typicode.com/photos`)
        })
      )
      .subscribe((data) => {
        console.log(data);
      });
  }
}
