import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewObservableComponent } from './components/new-observable/new-observable.component';
import { ConvertToObservableComponent } from './components/convert-to-observable/convert-to-observable.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CancelObservableComponent } from './components/cancel-observable/cancel-observable.component';
import { OperatorsComponent } from './components/operators/operators.component';
import { BufferComponent } from './components/buffer/buffer.component';
import { TakeComponent } from './components/take/take.component';
import { SkipComponent } from './components/skip/skip.component';
import { DistinctComponent } from './components/distinct/distinct.component';
import { FilterComponent } from './components/filter/filter.component';
import { SampleComponent } from './components/sample/sample.component';
import { AuditComponent } from './components/audit/audit.component';
import { ThrottleComponent } from './components/throttle/throttle.component';
import { FirstComponent } from './components/first/first.component';
import { DebounceComponent } from './components/debounce/debounce.component';
import { ElementAtComponent } from './components/element-at/element-at.component';
import { IgnoreElementsComponent } from './components/ignore-elements/ignore-elements.component';
import { SingleComponent } from './components/single/single.component';
import { MapComponent } from './components/map/map.component';
import { AjaxComponent } from './components/ajax/ajax.component';
import { HigherOrderComponent } from './components/higher-order/higher-order.component';
import { SubjectsComponent } from './components/subjects/subjects.component';
import { ErrorHandlingComponent } from './components/error-handling/error-handling.component';
import { JoinCreationOperatorsComponent } from './components/join-creation-operators/join-creation-operators.component';
import { SchedulersComponent } from './components/schedulers/schedulers.component';
import { MathOperatorsComponent } from './components/math-operators/math-operators.component';
import { UtilityOperatorsComponent } from './components/utility-operators/utility-operators.component';
import { TransformationComponent } from './components/transformation/transformation.component';

@NgModule({
  declarations: [
    AppComponent,
    NewObservableComponent,
    ConvertToObservableComponent,
    DashboardComponent,
    CancelObservableComponent,
    OperatorsComponent,
    BufferComponent,
    TakeComponent,
    SkipComponent,
    DistinctComponent,
    FilterComponent,
    SampleComponent,
    AuditComponent,
    ThrottleComponent,
    FirstComponent,
    DebounceComponent,
    ElementAtComponent,
    IgnoreElementsComponent,
    SingleComponent,
    MapComponent,
    AjaxComponent,
    HigherOrderComponent,
    SubjectsComponent,
    ErrorHandlingComponent,
    JoinCreationOperatorsComponent,
    SchedulersComponent,
    MathOperatorsComponent,
    UtilityOperatorsComponent,
    TransformationComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
